import Phaser from 'phaser'
import { centerGameObjects } from '../utils'

export default class extends Phaser.State {
  init() {}

  preload() {
    this.loaderBg = this.add.sprite(this.game.world.centerX, this.game.world.centerY, 'loaderBg')
    this.loaderBar = this.add.sprite(this.game.world.centerX, this.game.world.centerY, 'loaderBar')
    centerGameObjects([this.loaderBg, this.loaderBar])

    this.load.setPreloadSprite(this.loaderBar)
    //
    // load your assets
    //
    this.load.image('mushroom', 'assets/images/mushroom2.png');
    this.load.image('background', 'assets/images/BG.png');
    this.load.image('background2', 'assets/images/BG02.png');
    this.load.image('bullseye1', 'assets/images/Target01.png');
    this.load.image('bullseye2', 'assets/images/Target02.png');
    this.load.image('bullseye3', 'assets/images/Target03.png');
    this.load.image('bullseye4', 'assets/images/Target04.png');
    this.load.image('bullseyeCorrect', 'assets/images/TargetRight.png');
    this.load.image('bullseyeWrong', 'assets/images/TargetWrong.png');
    this.load.image('crosshair', 'assets/images/crosshair2.png');
    this.load.image('bulletHole1', 'assets/images/bulletHole1.png');
    this.load.image('bulletHole2', 'assets/images/bulletHole2.jpg');
    this.load.audio('gunshot', ['assets/sounds/gunshot.mp3', 'assets/sounds/shell.mp3']);
  }

  create() {
    this.state.start('Game')
  }
}
