/* globals __DEV__ */
import Phaser from 'phaser'
import Bullseye from '../sprites/Bullseye';
import Crosshair from '../sprites/Crossair';
import BulletHole from '../sprites/BulletHole';
import Background from '../sprites/Background';

export default class extends Phaser.State {
  
  init() {
    this.level = 1;
  }
  
  preload() { }
  
  create() {
    this.physics.startSystem(Phaser.Physics.ARCADE);
    const bannerText = 'Gamify!'
    let banner = this.add.text(this.world.centerX, this.game.height - 80, bannerText)
    banner.font = 'Bangers'
    banner.padding.set(10, 16)
    banner.fontSize = 40
    banner.fill = '#77BFA3'
    banner.smoothed = false
    banner.anchor.setTo(0.5);
    this.gunSound = this.game.sound.add('gunshot');
    this.background = new Background({
      game: this,
      x: 0,
      y: 0,
      width: 1024,
      height: 768,
      asset: 'background'
    });
    this.game.add.existing(this.background);

    this.crosshair = new Crosshair({
      game: this,
      x: this.world.centerX,
      y: this.world.centerY,
      asset: 'crosshair'
    });
    this.physics.enable(this.crosshair, Phaser.Physics.ARCADE);
    this.crosshair.body.allowRotation = false;
    
    this.targets = [];
    this.targetGroups = [];
    for (let i = 0; i < 4; i++) {
      this.targetGroups[i] = this.game.add.group();
      this.targets[i] = new Bullseye({
        game: this,
        x: this.world.centerX + (1000 * Math.random()),
        y: this.world.centerY - (1000 * Math.random()),
        asset: `bullseye${i + 1}`
      });
      
      this.physics.enable(this.targets[i], Phaser.Physics.ARCADE);
      
      this.setTargetMovement(this.targets[i]);
      
      this.targetGroups[i].add(this.targets[i]);
    }
    
    this.game.add.existing(this.crosshair);
    this.game.input.onDown.add(this.addBulletHole, this);
    
  }
  
  setTargetMovement(sprite) {
    sprite.body.velocity.setTo(300, 300);
    sprite.body.collideWorldBounds = true;
    sprite.body.bounce.set(0.99);
    sprite.body.gravity.set(120, 200);
  }
  
  addBulletHole(pointer) {
    this.gunSound.play();
    const { x, y } = this.crosshair.position;
    const bulletHole = new BulletHole({
      game: this,
      x,
      y,
      asset: 'bulletHole1'
    });
    let showBulletHole = true;
    for (let i = 0; i < this.targetGroups.length; i++) {
      if (this.targets[i].getBounds().contains(x, y)) {
        showBulletHole = false;
        if (i === 1) {
          this.targets[i].loadTexture('bullseyeCorrect');
          this.nextLevel();
        } else {
          this.targets[i].loadTexture('bullseyeWrong');
        }
        console.log('YES!!');
      }
    }
    if (showBulletHole) {
      this.game.add.existing(bulletHole);
    }
  }
  
  nextLevel() {
    setTimeout(() => {
      for (let i = 0; i < this.targets.length; i++) {
        this.targets[i].loadTexture(`bullseye${i + 1}`);
      }
      this.background.loadTexture('background2');
    }, 1000)
  }
  
  update() {
    // crosshair
    this.crosshair.rotation = this.physics.arcade.moveToPointer(this.crosshair, 60, this.input.activePointer, 500);
    
    // targets
    for (let i = 0; i < this.targets.length; i++) {
      for (let j = 0; j < this.targets.length; j++) {
        if (i !== j) {
          this.physics.arcade.collide(this.targets[i], this.targets[j]);
        }
      }
    }
  }
  
  render() {
    if (__DEV__) {
      // this.game.debug.spriteInfo(targets[0], 32, 32)
    }
  }
}
